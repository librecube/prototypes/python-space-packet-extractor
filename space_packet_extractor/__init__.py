import zmq

from sdlp import TelemetryTransferFrame, SpacePacket,\
    CommunicationsLinkControlWord


class SpacePacketExtractor:

    def __init__(self, space_packet_port, clcw_port):
        self._space_packet_port = space_packet_port
        self._clcw_port = clcw_port

        self._context = zmq.Context()
        self._space_packet_zmq_socket = self._context.socket(zmq.PUB)
        self._clcw_zmq_socket = self._context.socket(zmq.PUB)
        self._buffer = {}

    def bind(self):
        self._space_packet_zmq_socket.bind(
            "tcp://*:{}".format(self._space_packet_port))
        self._clcw_zmq_socket.bind(
            "tcp://*:{}".format(self._clcw_port))

    def unbind(self):
        self._space_packet_zmq_socket.close()
        self._clcw_zmq_socket.close()

    def _space_packet_indication(self, space_packet):
        self._space_packet_zmq_socket.send(space_packet.encode())

        print("Packet APID {} - Count {} - Data {}...".format(
            space_packet.primary_header.apid,
            space_packet.primary_header.packet_sequence_count,
            space_packet.data_field[:20].hex()
        ))

    def _clcw_indication(self, clcw):
        self._clcw_zmq_socket.send(clcw.encode())

        print("CLCW VC {} - Status {} - FarmB {}".format(
            clcw.virtual_channel_id,
            clcw.status_field,
            clcw.farm_b_counter
        ))

    def process_message(self, message):
        tmtf = TelemetryTransferFrame.decode(message.data)

        if tmtf.primary_header.ocf_flag:
            clcw = CommunicationsLinkControlWord.decode(
                tmtf.operational_control_field)
            self._clcw_indication(clcw)

        if tmtf.primary_header.sync_flag is True:
            raise ValueError("sync flag is set")

        sc_id = tmtf.primary_header.spacecraft_id
        vc_id = tmtf.primary_header.virtual_channel_id
        vc_count = tmtf.primary_header.virtual_channel_frame_count

        if (sc_id, vc_id) not in self._buffer:
            if tmtf.primary_header.first_header_pointer == 2046:
                # buffer empty, idle frame, skipping
                return
            elif tmtf.primary_header.first_header_pointer == 2047:
                # buffer empty, no new packet starts in frame, skipping
                return
            else:
                self._buffer[(sc_id, vc_id)] = {
                    'vc_count': vc_count,
                    'data': tmtf.frame_data_field[
                        tmtf.primary_header.first_header_pointer:]
                }
        else:
            expected_vc_count =\
                (self._buffer[(sc_id, vc_id)]['vc_count'] + 1) & 0xff
            if vc_count != expected_vc_count:
                # one or more frames lost, clear buffer and start again
                del self._buffer[(sc_id, vc_id)]
                self.process_message(message)
                return
            else:
                self._buffer[(sc_id, vc_id)]['vc_count'] = vc_count
                if tmtf.primary_header.first_header_pointer == 2046:
                    # idle frame, skipping
                    return
                elif tmtf.primary_header.first_header_pointer == 2047:
                    # append entire frame content to buffer
                    self._buffer[(sc_id, vc_id)]['data'] += tmtf.frame_data_field
                    return
                else:
                    # add remainder from previous frame to buffer
                    self._buffer[(sc_id, vc_id)]['data'] +=\
                        tmtf.frame_data_field[:tmtf.primary_header.first_header_pointer]
                    # extract remaining packets
                    self._extract_packets_from_buffer(self._buffer[(sc_id, vc_id)]['data'])
                    # start buffer new from first packet header
                    self._buffer[(sc_id, vc_id)]['data'] =\
                        tmtf.frame_data_field[tmtf.primary_header.first_header_pointer:]

        remaining = self._extract_packets_from_buffer(self._buffer[(sc_id, vc_id)]['data'])
        self._buffer[(sc_id, vc_id)]['data'] = remaining

    def _extract_packets_from_buffer(self, buffer):
        while True:
            try:
                space_packet = SpacePacket.decode(buffer)
                if space_packet.primary_header.apid == 0:
                    buffer = bytearray()
                    return buffer
            except Exception:
                return buffer
            # remove decoded space packet from buffer
            buffer = buffer[len(space_packet):]

            if space_packet.primary_header.apid == 2047:
                # idle packet, ignoring
                pass
            else:
                self._space_packet_indication(space_packet)
