import time

from nis_interface import TelemetryMessageInterfaceClient

from space_packet_extractor import SpacePacketExtractor


host = input("Host [172.16.173.130]: ")
host = host if host != "" else "172.16.173.130"

port = input("Port [13011]: ")
port = int(port) if port != "" else 13011

extractor = SpacePacketExtractor(23008, 23006)
extractor.bind()
client = TelemetryMessageInterfaceClient(host, port, version=0)
client.indication = extractor.process_message
client.connect()
client.start()

try:
    while True:
        time.sleep(1)
except KeyboardInterrupt:
    pass

client.stop()
client.disconnect()
extractor.unbind()
